// package fr.it_akademy.fx.sms;

// import java.io.BufferedReader;
// import java.io.DataOutputStream;
// import java.io.IOException;
// import java.io.InputStreamReader;
// import java.io.UnsupportedEncodingException;
// import java.net.HttpURLConnection;
// import java.net.MalformedURLException;
// import java.net.URL;
// import java.security.MessageDigest;
// import java.security.NoSuchAlgorithmException;
// import java.util.Date;
// import java.util.Scanner;

// /**
//  * Cette classe permet d'envoyer un SMS à l'aide de l'API d'OVH
//  * 
//  * https://help.ovhcloud.com/csm/fr-sms-sending-via-api-java?id=kb_article_view&sysparm_article=KB0051369
//  */
// public class App {

//     // Application Key
//     protected static String AK = "11228ee6afe1ca46";

//     // Application Secret
//     protected static String AS = "f75ab3207995c3a86ed52eb3a1618c20";

//     // Consumer Key
//     protected static String CK = "bf5e5a953feb816ddb83efc39a3117db";

//     private static String numeroDestinataire;
//     private static String contenuDuMessage;
//     private static Scanner scanner = new Scanner(System.in);

//     public static void main(String[] args) {
//         String nomDuService = recupererNomDuService();
// 		String METHOD = "GET";
// 		try {
// 			URL QUERY = new URL("https://eu.api.ovh.com/1.0/sms/");
// 			String BODY = "";
//             long TSTAMP = new Date().getTime() / 1000;

// 			String toSign = AS + "+" + CK + "+" + METHOD + "+" + QUERY + "+" + BODY + "+" + TSTAMP;
//             String signature = "$1$" + hashSHA1(toSign);
//             System.out.println(signature);

//             HttpURLConnection req = (HttpURLConnection) QUERY.openConnection();
//             req.setRequestMethod(METHOD);
//             req.setRequestProperty("Content-Type", "application/json");
//             req.setRequestProperty("X-Ovh-Application", AK);
//             req.setRequestProperty("X-Ovh-Consumer", CK);
//             req.setRequestProperty("X-Ovh-Signature", signature);
//             req.setRequestProperty("X-Ovh-Timestamp", "" + TSTAMP);

// 			String inputLine;
// 			BufferedReader in;
// 			int responseCode = req.getResponseCode();
// 			if (responseCode == 200) {
// 				// Récupération du résultat de l'appel
// 				in = new BufferedReader(new InputStreamReader(req.getInputStream()));
// 			} else {
// 				in = new BufferedReader(new InputStreamReader(req.getErrorStream()));
// 			}
// 			StringBuffer response = new StringBuffer();

// 			while ((inputLine = in.readLine()) != null) {
// 				response.append(inputLine);
// 			}
// 			nomDuService = response.toString();
// 			System.err.println(nomDuService);
// 			in.close();
// 		} catch (MalformedURLException e) {
//             e.printStackTrace();
//         } catch (IOException e) {
// 			e.printStackTrace();
// 		} 

//         System.out.println("Ce programme utilise le compte OVH : " + nomDuService);
//         demanderNumeroDestinataire();
//         demanderMessage();
//         envoyerSMS(nomDuService);
//         scanner.close();
//     }

//     private static String recupererNomDuService() {
//         // Implement logic to retrieve the service name
//         // For example: return someServiceName;
//         return "sms";
//     }

//     private static void envoyerSMS(String nomDuService) {
//         // TODO
//         String METHOD = "POST";
//         try {
//             URL QUERY = new URL("https://eu.api.ovh.com/1.0/sms/" + numeroDestinataire + "/jobs");
//             String BODY = contenuDuMessage;

//             long TSTAMP = new Date().getTime() / 1000;

//             // Création de la signature
//             String toSign = AS + "+" + CK + "+" + METHOD + "+" + QUERY + "+" + BODY + "+" + TSTAMP;
//             String signature = "$1$" + hashSHA1(toSign);
//             System.out.println(signature);

//             HttpURLConnection req = (HttpURLConnection) QUERY.openConnection();
//             req.setRequestMethod(METHOD);
//             req.setRequestProperty("Content-Type", "application/json");
//             req.setRequestProperty("X-Ovh-Application", AK);
//             req.setRequestProperty("X-Ovh-Consumer", CK);
//             req.setRequestProperty("X-Ovh-Signature", signature);
//             req.setRequestProperty("X-Ovh-Timestamp", "" + TSTAMP);

//             if (!BODY.isEmpty()) {
//                 req.setDoOutput(true);
//                 DataOutputStream wr = new DataOutputStream(req.getOutputStream());
//                 wr.writeBytes(BODY);
//                 wr.flush();
//                 wr.close();
//             }

//             String inputLine;
//             BufferedReader in;
//             int responseCode = req.getResponseCode();
//             if (responseCode == 200) {
//                 // Récupération du résultat de l'appel
//                 in = new BufferedReader(new InputStreamReader(req.getInputStream()));
//             } else {
//                 in = new BufferedReader(new InputStreamReader(req.getErrorStream()));
//             }
//             StringBuffer response = new StringBuffer();

//             while ((inputLine = in.readLine()) != null) {
//                 response.append(inputLine);
//             }
//             in.close();

//             // Affichage du résultat
//             System.out.println(response.toString());

//         } catch (MalformedURLException e) {
//             e.printStackTrace();
//         } catch (IOException e) {
//             e.printStackTrace();
//         }
//     }

//     public static String hashSHA1(String text) {
//         MessageDigest md;
//         try {
//             md = MessageDigest.getInstance("SHA-1");
//             byte[] sha1hash = new byte[40];
//             md.update(text.getBytes("iso-8859-1"), 0, text.length());
//             sha1hash = md.digest();
//             return convertToHex(sha1hash);
//         } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
//             e.printStackTrace();
//             return "";
//         }
//     }

//     private static String convertToHex(byte[] data) {
//         StringBuilder buf = new StringBuilder();
//         for (int i = 0; i < data.length; i++) {
//             int halfbyte = (data[i] >>> 4) & 0x0F;
//             int two_halfs = 0;
//             do {
//                 if ((0 <= halfbyte) && (halfbyte <= 9))
//                     buf.append((char) ('0' + halfbyte));
//                 else
//                     buf.append((char) ('a' + (halfbyte - 10)));
//                 halfbyte = data[i] & 0x0F;
//             } while (two_halfs++ < 1);
//         }
//         return buf.toString();
//     }

//     private static void demanderNumeroDestinataire() {
//         System.out.print("Veuillez saisir le numéro du destinataire (en débutant par +336 ou +337): ");
//         numeroDestinataire = scanner.nextLine();
//     }

//     private static void demanderMessage() {
//         System.out.print("Veuillez saisir votre message : ");
//         contenuDuMessage = scanner.nextLine();
//     }
// }



package fr.it_akademy.fx.sms;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.Scanner;

/**
 * Cette classe permet d'envoyer un SMS à l'aide de l'API d'OVH
 * 
 * https://help.ovhcloud.com/csm/fr-sms-sending-via-api-java?id=kb_article_view&sysparm_article=KB0051369
 */
public class App {

    // Application Key
    protected static String AK = "11228ee6afe1ca46";

    // Application Secret
    protected static String AS = "f75ab3207995c3a86ed52eb3a1618c20";

    // Consumer Key
    protected static String CK = "bf5e5a953feb816ddb83efc39a3117db";
	

    private static String numeroDestinataire;
    private static String contenuDuMessage;
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        String nomDuService = recupererNomDuService();
        System.out.println("Ce programme utilise le compte OVH : " + nomDuService);
        demanderNumeroDestinataire();
        demanderMessage();
         envoyerSMS(nomDuService);
        scanner.close();
    }

    private static String recupererNomDuService() {
        // Implement logic to retrieve the service name
        // For example: return someServiceName;
		String ServiceName = "sms-cf678891-1";
        return ServiceName;
    }
	// private static String recupererNomDuService() {
	// 	Scanner scanner = new Scanner(System.in);
	// 	System.out.print("Veuillez saisir le nom du service OVH : ");
	// 	return scanner.nextLine();
	// }+
	
    private static void envoyerSMS(String nomDuService) {
        // TODO
        String METHOD = "POST";
        try {
            URL QUERY = new URL("https://eu.api.ovh.com/1.0/sms/" + numeroDestinataire + "/jobs");
            String BODY = contenuDuMessage;

            long TSTAMP = new Date().getTime() / 1000;

            // Création de la signature
            String toSign = AS + "+" + CK + "+" + METHOD + "+" + QUERY + "+" + BODY + "+" + TSTAMP;
            String signature = "$1$" + hashSHA1(toSign);
            System.out.println(signature);

            HttpURLConnection req = (HttpURLConnection) QUERY.openConnection();
            req.setRequestMethod(METHOD);
            req.setRequestProperty("Content-Type", "application/json");
            req.setRequestProperty("X-Ovh-Application", AK);
            req.setRequestProperty("X-Ovh-Consumer", CK);
            req.setRequestProperty("X-Ovh-Signature", signature);
            req.setRequestProperty("X-Ovh-Timestamp", "" + TSTAMP);

            if (!BODY.isEmpty()) {
                req.setDoOutput(true);
                DataOutputStream wr = new DataOutputStream(req.getOutputStream());
                wr.writeBytes(BODY);
                wr.flush();
                wr.close();
            }

            String inputLine;
            BufferedReader in;
            int responseCode = req.getResponseCode();
            if (responseCode == 200) {
                // Récupération du résultat de l'appel
                in = new BufferedReader(new InputStreamReader(req.getInputStream()));
            } else {
                in = new BufferedReader(new InputStreamReader(req.getErrorStream()));
            }
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            // Affichage du résultat
            System.out.println(response.toString());

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String hashSHA1(String text) {
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("SHA-1");
            byte[] sha1hash = new byte[40];
            md.update(text.getBytes("iso-8859-1"), 0, text.length());
            sha1hash = md.digest();
            return convertToHex(sha1hash);
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            e.printStackTrace();
            return "";
        }
    }

    private static String convertToHex(byte[] data) {
        StringBuilder buf = new StringBuilder();
        for (int i = 0; i < data.length; i++) {
            int halfbyte = (data[i] >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                if ((0 <= halfbyte) && (halfbyte <= 9))
                    buf.append((char) ('0' + halfbyte));
                else
                    buf.append((char) ('a' + (halfbyte - 10)));
                halfbyte = data[i] & 0x0F;
            } while (two_halfs++ < 1);
        }
        return buf.toString();
    }

    private static void demanderNumeroDestinataire() {
        System.out.print("Veuillez saisir le numéro du destinataire (en débutant par +336 ou +337): ");
        numeroDestinataire = scanner.nextLine();
    }

    private static void demanderMessage() {
        System.out.print("Veuillez saisir votre message : ");
        contenuDuMessage = scanner.nextLine();
    }
}
