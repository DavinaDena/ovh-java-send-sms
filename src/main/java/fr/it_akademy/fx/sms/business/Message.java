package fr.it_akademy.fx.sms.business;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Message {
	
	private static final String PRIORITE_PAR_DEFAUT = "high";
	private static final boolean AUTORISE_REPONSE_PAR_DEFAUT = true;
	
	@JsonProperty("receivers")
	private List<String> destinataires;
	
	@JsonProperty("message")
	private String contenu;
	
	@JsonProperty("priority")
	private String priorite;
	
	@JsonProperty("senderForResponse")
	private boolean autoriseReponse;

	public Message() {
		priorite = PRIORITE_PAR_DEFAUT;
		autoriseReponse = AUTORISE_REPONSE_PAR_DEFAUT;
		destinataires = new ArrayList<>();
	}
		
}